# Shipmate CHANGELOG

## 1.5.3
* Initial version
* Improved backup help text
* Parameters are passed when using `docker stack deploy` so that `--prune` works

## 1.6.6
* Added support for `shipmate bump ENVIRONMENT [major|minor|patch|prerelease]`
* Added bash detection for `shipmate attach`
* Added `shipmate update`, this will install the latest version of shipmate to /usr/local/bin
* Added `shipmate versionsync ENVIRONMENT`, this sets the ENVIRONMENT.yml to the version in package.json

## 1.7.x
* More consistent command naming
* Prefix versions with 'v' when using `shipmate bump`
* Fix `shipmate build` handling of version format: `v1.2.3-tag`
* Added confirmation to deploy
* `shipmate destroy` is now `shipmate remove all` and also removes containers
* added `shipmate remove keepsecrets`
* Fixed 'Please tell me who you are' error caused by `npm version`
* Fixed backup/restore bug
* Added `shipmate remove containers`

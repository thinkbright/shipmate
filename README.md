# Shipmate

A nifty tool for managing docker stacks.

## Install to /usr/local/bin

```
curl -kL https://bitbucket.org/thinkbright/shipmate/get/master.tar.gz | \
tar -xzO "*/shipmate" > /usr/local/bin/shipmate && \
chmod +x /usr/local/bin/shipmate
```

## Update

```
shipmate update
```

## Usage

Shipmate expects either env settings or a .shipmaterc in the current working
directory with the following variables:

```
SM_CONFIG_DIR=[location of *.yml files]
SM_PACKAGEJSON_DIR=[location of node package.json]
SM_ORGANIZATION=[name of your organization]
SM_STACK=[name of your docker stack]
```

Create one with the following script:

```
shipmate init
```

## Commands

```
Shipmate: a nifty tool for managing docker stacks

Usage:  shipmate COMMAND

Commands:
  attach      Attach to a container to run commands
  backup      Backup a [database]
  build	      Build the current repository as provider
  bump	      Updates versions in package.json and ENVIRONMENT.yml
  deploy      Deploy the stack to swarm
  exec	      Execute a command on containers in the stack
  init	      Create a .shipmaterc
  log	        Log output from services in the stack
  list	      List containers, secrets, services or volumes in the stack
  ps	        List the tasks in the stack
  restart     Restart a service in the stack
  restore     Restore a [database]
  remove      Remove containers, stack, secrets, volumes or all togehter
  update      Download the latest version of shipmate to /usr/local/bin
  versionsync	Updates ENVIRONMENT.yml with the version in package.json

```